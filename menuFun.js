var today = new Date();
var monthArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
var monthDateArray = [31,28,31,30,31,30,31,31,30,31,30,31];
var districtArray = ['Central and Western', 'Eastern', 'Islands', 'Kowloon City', 'Kwai Tsing', 'Kwun Tong', 'North', 'Sai Kung', 'Sha Tin', 'Sham Shui Po', 'Southern', 'Tai Po', 'Tsuen Wan', 'Tuen Mun', 'Wan Chai', 'Wong Tai Sin', 'Yau Tsim Mong', 'Yuen Long']
var selDate;
var calDate;
function loadCal(calMM, calYYYY, selDD, selMM, selYYYY){ //mm: 0 - 11. 0 : Jan
	selDate = new Date(monthArray[selMM] + ' ' + selDD + ', ' + selYYYY);
	calDate = new Date(monthArray[calMM] + ' 1, ' + calYYYY);
	var monthDate = monthDateArray[calDate.getMonth()];
	if (calDate.getMonth() == 1 && calDate.getFullYear()%4 == 0){
		monthDate = 29;
	}
	var content = "";
	for (var i = 0; i < calDate.getDay(); i++){
		content += "<li></li>";
	}
	for (var i = 1; i <= monthDate; i++){
		content += '<li onClick="loadCal(' + calMM + ', ' + calYYYY + ', ' + i + ', ' + calMM + ', ' + calYYYY + ')">';
		//today is in Cal
		if (calYYYY == today.getFullYear() && calMM == today.getMonth() && i == today.getDate()){
			content += '<span class="instant">';
		}
		//selected date is in Cal
		if (calYYYY == selYYYY && calMM == selMM && i == selDD){
			content += '<span class="selected">';
		}
		content += i;
		if (calYYYY == today.getFullYear() && calMM == today.getMonth() && i == today.getDate()){
			content += '</span>';
		}
		if (calYYYY == selYYYY && calMM == selMM && i == selDD){
			content += '</span>';
		}
		content += '</li>';
	}
	var nextMM = ((calDate.getMonth()==11) ? 0 : calDate.getMonth()+1);
	var nextYYYY = ((calDate.getMonth()==11) ? calDate.getFullYear()+1 : calDate.getFullYear());
	var prevMM = ((calDate.getMonth()==0) ? 11 : calDate.getMonth()-1);
	var prevYYYY = ((calDate.getMonth()==0) ? calDate.getFullYear()-1 : calDate.getFullYear());
	document.getElementById('Calendar').getElementsByClassName('head')[0].innerHTML = '<li class="prev" onClick="loadCal(' + prevMM + ', ' + prevYYYY + ', ' + selDD + ', ' + selMM + ', ' + selYYYY +')">Prev</li>' + '<li class="next" onClick="loadCal(' + nextMM + ', ' + nextYYYY + ', ' + selDD + ', ' + selMM + ', ' + selYYYY + ')">Next</li><li style="text-align:center">' + monthArray[calMM] + '<br><span style="font-size:15px">' + calYYYY + '</span></li>';
	document.getElementById('Calendar').getElementsByClassName('days')[0].innerHTML = content;
	console.log(document.getElementById('Calendar').getElementsByClassName('head'));
	//console.log(startDate.getDay());
}

function initCal(){
	document.getElementById('Calendar').innerHTML = '<ul class="head"></ul><ul class="weekdays"><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul><ul class="days"></ul>';
	loadCal(today.getMonth(),today.getFullYear(),today.getDate(),today.getMonth(),today.getFullYear());
	var hr = today.getHours();
	if (hr >= 0 && hr < 3){
		document.getElementById('timeSectionsel').innerHTML = '00:00-03:00';
		document.getElementById('timeSectionsel').value = '0000';
	} else if (hr >= 3 && hr < 6){
		document.getElementById('timeSectionsel').innerHTML = '03:00-06:00';
		document.getElementById('timeSectionsel').value = '0300';
	} else if (hr >= 6 && hr < 9){
		document.getElementById('timeSectionsel').innerHTML = '06:00-09:00';
		document.getElementById('timeSectionsel').value = '0600';
	} else if (hr >= 9 && hr < 12){
		document.getElementById('timeSectionsel').innerHTML = '09:00-12:00';
		document.getElementById('timeSectionsel').value = '0900';
	} else if (hr >= 12 && hr < 15){
		document.getElementById('timeSectionsel').innerHTML = '12:00-15:00';
		document.getElementById('timeSectionsel').value = '1200';
	} else if (hr >= 15 && hr < 18){
		document.getElementById('timeSectionsel').innerHTML = '15:00-18:00';
		document.getElementById('timeSectionsel').value = '1500';
	} else if (hr >= 18 && hr < 21){
		document.getElementById('timeSectionsel').innerHTML = '18:00-21:00';
		document.getElementById('timeSectionsel').value = '1800';
	} else if (hr >= 21 && hr < 24){
		document.getElementById('timeSectionsel').innerHTML = '21:00-00:00';
		document.getElementById('timeSectionsel').value = '2100';
	}
	for (i in districtArray){
		document.getElementById('distItem').innerHTML += '<a href="#" onClick="distSel.innerHTML=\'' + districtArray[i] + '\'">' + districtArray[i] + '</a>'
	}
	document.getElementById('searchmenu').className='show';
	document.getElementById('Button').addEventListener('click', function(){
		var DD;
		var MM;
		if (selDate.getMonth()+1 < 10){
			MM = '0' + selDate.getMonth()+1;
		} else {
			MM = selDate.getMonth()+1;
		}
		if (selDate.getDate() < 10){
			DD = '0' + selDate.getDate();
		} else {
			DD = selDate.getDate();
		}
        var normal_time ='2015'+MM+DD+document.getElementById('timeSectionsel').value;
    	var sel_Time = _norm2Unix('2015'+MM+DD+document.getElementById('timeSectionsel').value);
        console.log("sel_time = ", sel_Time);
    	clustering(normal_time,document.getElementById('distSel').innerHTML);
   	});
}

function _norm2Unix(normTime){ 
//in String format as ('yyyymmddhhmm')
  var timeStamp_start = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + normTime.slice(6,8) + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  var timeStamp_end = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + (parseInt(normTime.slice(6,8))+0).toString() + " " + (parseInt(normTime.slice(8,10))+3).toString() + ":" + normTime.slice(10,12);
  //console.log(normTime,Date.parse(timeStamp_start+" +0800")/1000);
  return {start: Date.parse(timeStamp_start+" +0800")/1000,
        end: Date.parse(timeStamp_end+" +0800")/1000
        };
}





window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    for (var d = 0; d < dropdowns.length; d++) {
      var openDropdown = dropdowns[d];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

