var sel_date = new Date();
var sel_time;
var sel_dist;
var monthArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
var weekdayArray = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
var districtArray = ['Central and Western', 'Eastern', 'Islands', 'Kowloon City', 'Kwai Tsing', 'Kwun Tong', 'North', 'Sai Kung', 'Sha Tin', 'Sham Shui Po', 'Southern', 'Tai Po', 'Tsuen Wan', 'Tuen Mun', 'Wan Chai', 'Wong Tai Sin', 'Yau Tsim Mong', 'Yuen Long'];
var districtcenter = {'Central and Western': new google.maps.LatLng(22.279995, 114.142822), 'Eastern': new google.maps.LatLng(22.276532, 114.221233), 'Islands': new google.maps.LatLng(22.288837, 113.938806), 'Kowloon City': new google.maps.LatLng(22.324706, 114.184261), 'Kwai Tsing': new google.maps.LatLng(22.351882, 114.113457), 'Kwun Tong': new google.maps.LatLng(22.316593, 114.231986), 'North': new google.maps.LatLng(22.509961, 114.154696), 'Sai Kung': new google.maps.LatLng(22.351375, 114.235030) , 'Sha Tin': new google.maps.LatLng(22.400086, 114.185831), 'Sham Shui Po': new google.maps.LatLng(22.333633, 114.153574), 'Southern': new google.maps.LatLng(22.234802, 114.206653), 'Tai Po': new google.maps.LatLng(22.446616, 114.153225), 'Tsuen Wan': new google.maps.LatLng(22.385228, 114.077848), 'Tuen Mun': new google.maps.LatLng(22.397678, 113.977914), 'Wan Chai': new google.maps.LatLng(22.269438, 114.178358), 'Wong Tai Sin': new google.maps.LatLng(22.342826, 114.200899), 'Yau Tsim Mong': new google.maps.LatLng(22.314676, 114.168973), 'Yuen Long': new google.maps.LatLng(22.463065, 114.025394)};
var districtInfo = {'Central and Western':{'population': 251519,'income': 40000}, 'Eastern':{'population': 588094,'income': 30000} , 'Islands':{'population': 141327,'income': 24000} , 'Kowloon City':{'population': 377351,'income': 29070} , 'Kwai Tsing':{'population': 511167,'income': 20000} , 'Kwun Tong':{'population': 622152,'income': 20000} , 'North':{'population': 304134,'income': 21500} , 'Sai Kung':{'population': 436627,'income': 30000} , 'Sha Tin':{'population': 630273,'income': 26800} , 'Sham Shui Po':{'population': 380855,'income': 20800} , 'Southern':{'population': 278655,'income': 30000} , 'Tai Po':{'population': 296853,'income': 25500} , 'Tsuen Wan':{'population': 304637,'income': 28830} , 'Tuen Mun':{'population': 487546,'income': 21000} , 'Wan Chai':{'population': 152608,'income': 45200} , 'Wong Tai Sin':{'population': 420183,'income': 20900} , 'Yau Tsim Mong':{'population': 307878,'income': 27880} , 'Yuen Long':{'population': 578529,'income': 20900}};
var wholeDistInfo={'population': 7071576,'income': 24810}
$(function () {

  'use strict';

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  // Get context with jQuery - using jQuery's .get() method.
  /*var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        label: "Electronics",
        fillColor: "rgb(210, 214, 222)",
        strokeColor: "rgb(210, 214, 222)",
        pointColor: "rgb(210, 214, 222)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56, 55, 40]
      },
      {
        label: "Digital Goods",
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgba(60,141,188,0.8)",
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: false,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: false,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);
  */
  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------

  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  /*var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: 700,
      color: "#f56954",
      highlight: "#f56954",
      label: "Chrome"
    },
    {
      value: 500,
      color: "#00a65a",
      highlight: "#00a65a",
      label: "IE"
    },
    {
      value: 400,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "FireFox"
    },
    {
      value: 600,
      color: "#00c0ef",
      highlight: "#00c0ef",
      label: "Safari"
    },
    {
      value: 300,
      color: "#3c8dbc",
      highlight: "#3c8dbc",
      label: "Opera"
    },
    {
      value: 100,
      color: "#d2d6de",
      highlight: "#d2d6de",
      label: "Navigator"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=value %> <%=label%> users"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  //-----------------
  //- END PIE CHART -
  //-----------------
*/
  
  
  //Calendar
  $("#calendar").datepicker('update', new Date(sel_date.getFullYear(), sel_date.getMonth(),sel_date.getDate()))
    .on('changeDate', function(e) {
        sel_date = $("#calendar").datepicker('getDate');
        console.log(sel_date);
    });
    //console.log((sel_date.getDate()+1) + '/' + (sel_date.getMonth()+1) + '/' + (sel_date.getFullYear()) + ' ' + sel_time.slice(0,1) + ':' + sel_time.slice(2,3));
    //$("#sel_options").innerHTML = (sel_date.getDate()+1) + '/' + (sel_date.getMonth()+1) + '/' + (sel_date.getFullYear()) + ' ' + sel_time.slice(0,2) + ':' + sel_time.slice(2,4);

  //time section button auto_sel
  var hr = sel_date.getHours();
  if (hr >= 0 && hr < 3){
    $('#0000').toggleClass('selected');
    sel_time = '0000';
  } else if (hr >= 3 && hr < 6){
    $('#0300').toggleClass('selected');
    sel_time = '0300';
  } else if (hr >= 6 && hr < 9){
    $('#0600').toggleClass('selected');
    sel_time = '0600';
  } else if (hr >= 9 && hr < 12){
    $('#0900').toggleClass('selected');
    sel_time = '0900';
  } else if (hr >= 12 && hr < 15){
    $('#1200').toggleClass('selected');
    sel_time = '1200';
  } else if (hr >= 15 && hr < 18){
    $('#1500').toggleClass('selected');
    sel_time = '1500';
  } else if (hr >= 18 && hr < 21){
    $('#1800').toggleClass('selected');
    sel_time = '1800';
  } else if (hr >= 21 && hr < 24){
    $('#2100').toggleClass('selected');
    sel_time = '2100';
  }
  for (var district in districtArray){
    document.getElementById('district').innerHTML += '<a href="javascript:void(0)" class="btn btn-default" id="' + districtArray[district] + '" onClick="change_dist(\'' + districtArray[district] + '\')" style="margin-right:1px; margin-bottom: 1px; width: 15%">' + districtArray[district] + '</a>';
  }
  sel_dist = 'Central and Western';
  document.getElementById(sel_dist).className = 'btn btn-default selected';

  document.getElementById('sel_options').innerHTML = sel_date.getDate() + ' ' + monthArray[sel_date.getMonth()] + ', ' + sel_date.getFullYear() + ' (' + weekdayArray[sel_date.getDay()] + ') ' + document.getElementById(sel_time).innerHTML + ' in ' + sel_dist + ' district';
  save_setting();
  //map ends
  /* jVector Maps
   * ------------
   * Create a world map with markers
   */
   /*
  $('#world-map-markers').vectorMap({
    map: 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity: 0.7,
    hoverColor: false,
    backgroundColor: 'transparent',
    regionStyle: {
      initial: {
        fill: 'rgba(210, 214, 222, 1)',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      },
      hover: {
        "fill-opacity": 0.7,
        cursor: 'pointer'
      },
      selected: {
        fill: 'yellow'
      },
      selectedHover: {}
    },
    markerStyle: {
      initial: {
        fill: '#00a65a',
        stroke: '#111'
      }
    },
    markers: [
      {latLng: [41.90, 12.45], name: 'Vatican City'},
      {latLng: [43.73, 7.41], name: 'Monaco'},
      {latLng: [-0.52, 166.93], name: 'Nauru'},
      {latLng: [-8.51, 179.21], name: 'Tuvalu'},
      {latLng: [43.93, 12.46], name: 'San Marino'},
      {latLng: [47.14, 9.52], name: 'Liechtenstein'},
      {latLng: [7.11, 171.06], name: 'Marshall Islands'},
      {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
      {latLng: [3.2, 73.22], name: 'Maldives'},
      {latLng: [35.88, 14.5], name: 'Malta'},
      {latLng: [12.05, -61.75], name: 'Grenada'},
      {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
      {latLng: [13.16, -59.55], name: 'Barbados'},
      {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
      {latLng: [-4.61, 55.45], name: 'Seychelles'},
      {latLng: [7.35, 134.46], name: 'Palau'},
      {latLng: [42.5, 1.51], name: 'Andorra'},
      {latLng: [14.01, -60.98], name: 'Saint Lucia'},
      {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
      {latLng: [1.3, 103.8], name: 'Singapore'},
      {latLng: [1.46, 173.03], name: 'Kiribati'},
      {latLng: [-21.13, -175.2], name: 'Tonga'},
      {latLng: [15.3, -61.38], name: 'Dominica'},
      {latLng: [-20.2, 57.5], name: 'Mauritius'},
      {latLng: [26.02, 50.55], name: 'Bahrain'},
      {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
    ]
  });
*/

  /* SPARKLINE CHARTS
   * ----------------
   * Create a inline charts with spark line
   */

  //-----------------
  //- SPARKLINE BAR -
  //-----------------
  $('.sparkbar').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'bar',
      height: $this.data('height') ? $this.data('height') : '30',
      barColor: $this.data('color')
    });
  });

  //-----------------
  //- SPARKLINE PIE -
  //-----------------
  $('.sparkpie').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'pie',
      height: $this.data('height') ? $this.data('height') : '90',
      sliceColors: $this.data('color')
    });
  });

  //------------------
  //- SPARKLINE LINE -
  //------------------
  $('.sparkline').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'line',
      height: $this.data('height') ? $this.data('height') : '90',
      width: '100%',
      lineColor: $this.data('linecolor'),
      fillColor: $this.data('fillcolor'),
      spotColor: $this.data('spotcolor')
    });
  });
});

function change_time(value)
  {
    $('#'+sel_time).toggleClass('selected');
    sel_time = value;
    $('#'+sel_time).toggleClass('selected');
  }

function change_dist(value)
  {
    document.getElementById(sel_dist).className = 'btn btn-default';
    sel_dist = value;
    document.getElementById(sel_dist).className = 'btn btn-default selected';
  }

function save_setting(){
  $('#Opt_collapse').click();
  console.log(sel_date);
  document.getElementById('sel_options').innerHTML = sel_date.getDate() + ' ' + monthArray[sel_date.getMonth()] + ', ' + sel_date.getFullYear() + ' (' + weekdayArray[sel_date.getDay()] + ') ' + document.getElementById(sel_time).innerHTML + ' in ' + sel_dist + ' district';
  //document.getElementById('param-district').innerHTML = sel_dist;
  //document.getElementById('param-date').innerHTML = sel_date.getDate() + ' ' + monthArray[sel_date.getMonth()] + ', ' + sel_date.getFullYear() + ' (' + weekdayArray[sel_date.getDay()] + ') ';
  //document.getElementById('param-time').innerHTML = document.getElementById(sel_time).innerHTML;
  document.getElementById('distInfoTitle').innerHTML = sel_dist;
  document.getElementById('population').innerHTML = districtInfo[sel_dist].population;
  document.getElementById('population bar').style.width = (Math.round((0.5+(districtInfo[sel_dist].population-(wholeDistInfo.population/18))/(wholeDistInfo.population/18)/2)*100)) + '%';
  document.getElementById('population desc').innerHTML = Math.round(districtInfo[sel_dist].population/wholeDistInfo.population*1000)/10 + '% of HK population';
  document.getElementById('income').innerHTML = districtInfo[sel_dist].income + 'HKD';
    var DD;
    var MM;
    if ((sel_date.getMonth()+1) < 10){
      MM = '0' + (sel_date.getMonth()+1);
    } else {
      MM = (sel_date.getMonth()+1);
    }
    if (sel_date.getDate() < 10){
      DD = '0' + sel_date.getDate();
    } else {
      DD = sel_date.getDate();
    }
    var sel_datetime = _norm2Unix('2015'+MM+DD+sel_time);
    console.log(sel_datetime);
    map.setZoom(14);
    map.setCenter(districtcenter[sel_dist]);
    clustering(sel_datetime,sel_dist);
    rain_ride();
}

function _norm2Unix(normTime){ 
//in String format as ('yyyymmddhhmm')
  var timeStamp_start = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + normTime.slice(6,8) + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  var timeStamp_end = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + (parseInt(normTime.slice(6,8))+0).toString() + " " + (parseInt(normTime.slice(8,10))+2).toString() + ":" + (parseInt(normTime.slice(10,12))+59).toString();
  console.log(timeStamp_end);
  return {start: Date.parse(timeStamp_start+" +0800")/1000,
        end: Date.parse(timeStamp_end+" +0800")/1000
        };
}
