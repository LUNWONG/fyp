var centroid = {};
var markerSet = [];
var circleSet = [];
var mapLabelSet = [];
var chosenCluster;
var markerInterval;
var markerTimeout;
var clusterColor = ['#ffff00','#66ff33','#ff33cc','#ff0000'];
var toDistChart = new Morris.Donut({
  element: 'toDistChart',
  resize: true,
  colors: ["#ffcc00", "#458B74", "#838B8B", "#0000FF", "#8A2BE2", "#FF0000", "#7FFF00", "#D2681E", "#6495ED", "#996633", "#000000", "#8470FF", "#FF00FF", "#0000CD", "#3CB371", "#C71585", "#191970", "#8B2500"],
  data: [
    {label: "Download Sales", value: 12},
    {label: "In-Store Sales", value: 30},
    {label: "Mail-Order Sales", value: 20}
  ],
  hideHover: 'auto'
});
var TaxiTable = $('#TaxiCallTable').DataTable({
                  "paging": true,
                  "lengthChange": false,
                  "searching": false,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false
                });
function clustering(sel_Time,sel_Dist){
    if (chosenCluster) {
      chosenCluster.setMap(null);
      chosenCluster = null;
      document.getElementById('clusterOverlayN').className='overlay';
    }
    if (document.getElementById('ClusterBox').className == 'box box-success'){
      document.getElementById('ClusData_collapse').click();
    }
    document.getElementById('mapOverlay').className = 'overlay';
    document.getElementById('tableOverlay').className = 'overlay';
    document.getElementById('mapOverlayF').className = 'overlay hidden';
    document.getElementById('tableOverlayF').className = 'overlay hidden';
    for (var i in markerSet){
      markerSet[i].setMap(null);
    }
    markerSet = [];
    for (var i in circleSet){
      circleSet[i].setMap(null);
    }
    circleSet = [];
    for (var i in circleSet){
      mapLabelSet[i].setMap(null);
    }
    mapLabelSet = [];
    centroid = {};
    var selected_district = sel_Dist;
//    var selected_time = norm2Unix("201502032100");
    var time_ranges= sel_Time;
    var startT = new Date(sel_Time.start*1000);
    var MM;
    if ( (startT.getMonth()+1) < 10){
      MM = '0' +  (startT.getMonth()+1);
    } else {
      MM =  (startT.getMonth()+1);
    }
    
    jQuery.getJSON("../data/district-2015-" + MM + "-data.json", function(data){
        var dbscan_input = [];
        var filtered_data = [];
        var temp = "";
        jQuery.each(data, function(key, val){
            if(val.fromdistrict === selected_district && inRange(time_ranges, parseInt(val.createdTime)) && val.fromdistrict!=null && val.todistrict!=null){
                corrdinates={};
                corrdinates.location = {};
                corrdinates.location.id = val.orderCreatorID;
                corrdinates.location.accuracy = 1;
                corrdinates.location.latitude = val.fromLocation.lat;
                corrdinates.location.longitude = val.fromLocation.lng;
                dbscan_input.push(corrdinates);
                val.distant = _calDirectDist(parseFloat(val.fromLocation.lat), parseFloat(val.fromLocation.lng), parseFloat(val.toLocation.lat), parseFloat(val.toLocation.lng));
                //console.log(val.distant);
                val.fee = _calFee(val.distant);
                filtered_data.push(val);
                temp += "<tr><td>" + val.fromdistrict + "</td><td>" + val.todistrict + "</td><td>" + Math.round(val.distant*10)/10 + "</td><td>" + val.fee + "</td><td>" + val.status + "</td></tr>";

                
            }
        });
        if (temp){
          if (TaxiTable){
            TaxiTable.destroy();
          }
          document.getElementById('CallData').innerHTML = temp;
          TaxiTable = $('#TaxiCallTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
          });
        }

        //######################################################
        //
        // Running DBSCAN algorithm
        var dbscanner = jDBSCAN().eps(0.325).minPts(5).distance('HAVERSINE').data(dbscan_input);
        var point_assg_result = dbscanner();
        console.log("total number of data: ", filtered_data.length);


        //#####################################################
        //
        // Finding the centroid of clusters
        // Pushing the data into the corresponding cluster
        for (var i=0; i<point_assg_result.length; i++){
            if ([point_assg_result[i]] != 0 ){ // 0 means the data is not assigned to any cluster
                if (centroid[point_assg_result[i]]){
                    centroid[point_assg_result[i]].num++;
                    if (filtered_data[i].status == "Done"){
                        centroid[point_assg_result[i]].done ++;
                    }
                    centroid[point_assg_result[i]].cen_lat += parseFloat(dbscan_input[i].location.latitude);
                    centroid[point_assg_result[i]].cen_lng += parseFloat(dbscan_input[i].location.longitude);
                    centroid[point_assg_result[i]].data.push(filtered_data[i]);
                }else{
                    centroid[point_assg_result[i]] = {};
                    centroid[point_assg_result[i]].num = 1;
                    if (filtered_data[i].status == "Done"){
                        centroid[point_assg_result[i]].done = 1;
                    }
                    else {
                        centroid[point_assg_result[i]].done = 0;
                    }
                    centroid[point_assg_result[i]].cen_lat = parseFloat(dbscan_input[i].location.latitude);
                    centroid[point_assg_result[i]].cen_lng = parseFloat(dbscan_input[i].location.longitude);
                    centroid[point_assg_result[i]].data = [];
                    centroid[point_assg_result[i]].data.push(filtered_data[i]);
                }

              var image;
              if (filtered_data[i].status == "Done"){
                image = 'image/marker-green.png';
              } else if (filtered_data[i].status == "DriverCancelled"){
                image = 'image/marker-yellow.png';
              } else if (filtered_data[i].status == "NoCar"){
                image = 'image/marker-purple.png';
              } else if (filtered_data[i].status == "UserCancelled"){
                image = 'image/marker-red.png';
              }
              var marker = new google.maps.Marker({
                    position: {lat: parseFloat(dbscan_input[i].location.latitude), lng: parseFloat(dbscan_input[i].location.longitude)},
                    icon: image
                });
              markerSet.push(marker);
            } else {
              var image;
              if (filtered_data[i].status == "Done"){
                image = 'image/taxi-green.png';
              } else if (filtered_data[i].status == "DriverCancelled"){
                image = 'image/taxi-yellow.png';
              } else if (filtered_data[i].status == "NoCar"){
                image = 'image/taxi-purple.png';
              } else if (filtered_data[i].status == "UserCancelled"){
                image = 'image/taxi-red.png';
              }
              var marker = new google.maps.Marker({
                    position: {lat: parseFloat(dbscan_input[i].location.latitude), lng: parseFloat(dbscan_input[i].location.longitude)},
                    icon: image
                });
              markerSet.push(marker);
            }
        }
        


        //######################################################
        //
        // Calculate the real centroid position
        for (var j in centroid){
            centroid[j].cen_lat = centroid[j].cen_lat/centroid[j].num;
            centroid[j].cen_lng = centroid[j].cen_lng/centroid[j].num;
        }


        //######################################################
        //
        // Plot the clusters circle and Find the distance of each order
        var info =[];
        var realMaxCluster = 0;
        var maxNumCluster = 0;
        var minNumCluster = 100000;
        var avgNumCluster = 0;
        var totalNumCluster = 0;
        for (var k in centroid){
          totalNumCluster += centroid[k].num;
        }
        avgNumCluster = totalNumCluster/(Object.keys(centroid).length);
        for (var k in centroid){
          if (centroid[k].num > maxNumCluster && ((centroid[k].num-avgNumCluster)/centroid[k].num) < 0.5){
            maxNumCluster = centroid[k].num;
          }
          if (centroid[k].num > realMaxCluster){
            realMaxCluster = centroid[k].num;
          }
          if (centroid[k].num < minNumCluster){
            minNumCluster = centroid[k].num;
          }
        }
        if (Object.keys(centroid).length==0){
          document.getElementById('clustMin').style.width = '100%';
          document.getElementById('clustMax').style.width = '0%';
          document.getElementById('clustMin').innerHTML = 'No Cluster Found';
          document.getElementById('clustMax').innerHTML = '';
        } else {
          document.getElementById('clustMin').style.width = '10%';
          document.getElementById('clustMax').style.width = '30%';
          document.getElementById('clustMin').innerHTML = minNumCluster;
          document.getElementById('clustMax').innerHTML = '>= ' + maxNumCluster;
        }
        console.log("centroid = ", centroid);
          var max_AvgDist = 0;
          for (var i in centroid){
            var sel_color;
            var stroke_color = '#000000';
            var stroke_weight = 1;
            if (Object.keys(centroid).length == 1 || minNumCluster == maxNumCluster){
              sel_color = clusterColor[3];
            } else {
              if ((centroid[i].num-minNumCluster)/(maxNumCluster-minNumCluster)*100 > 100){
                stroke_color = '#1a1aff';
                stroke_weight = 4;
              }
              if ((centroid[i].num-minNumCluster)/(maxNumCluster-minNumCluster)*100 >= 75){
                sel_color = clusterColor[3];
              } else if ((centroid[i].num-minNumCluster)/(maxNumCluster-minNumCluster)*100 >= 50){
                sel_color = clusterColor[2];
              } else if ((centroid[i].num-minNumCluster)/(maxNumCluster-minNumCluster)*100 >= 25){
                sel_color = clusterColor[1];
              } else if ((centroid[i].num-minNumCluster)/(maxNumCluster-minNumCluster)*100 >= 0){
                sel_color = clusterColor[0];
              }
            }
            circle = drawCircle({lat: centroid[i].cen_lat, lng: centroid[i].cen_lng}, 375, map,sel_color,stroke_color,stroke_weight);
            circleSet.push(circle);
            var Cmarker = new google.maps.Marker({
              position: {lat: centroid[i].cen_lat, lng: centroid[i].cen_lng},
              opacity: 1,
              icon: 'image/chosen.png'
            });
            centroid[i].toLocation = {};
            for (var j = 0; j<centroid[i].data.length; j++){
                if(centroid[i].toLocation[centroid[i].data[j].todistrict]){
                    centroid[i].toLocation[centroid[i].data[j].todistrict]++;
                }
                else{
                    centroid[i].toLocation[centroid[i].data[j].todistrict] = 1;
                }
            }
            centroid[i].marker = Cmarker;
            //centroid[i].infowindow = makeInfoWin(centroid[i]);
            //addInfoBox(circle, {lat: centroid[i].cen_lat, lng: centroid[i].cen_lng}, centroid[i]);
            addClusterInfo(circle, centroid[i], maxNumCluster, {lat: centroid[i].cen_lat, lng: centroid[i].cen_lng});
            var mapLabel = new MapLabel({
              text: centroid[i].num,
              position: new google.maps.LatLng(centroid[i].cen_lat+0.0009, centroid[i].cen_lng-0.0005),
              map: map,
              fontSize: 16,
              align: 'center'
            });
            mapLabelSet.push(mapLabel);

            //calculate the average distance of the cluster
            var totalDist = 0;
            for (var j = 0; j< centroid[i].num; j++){
                var lat1 = parseFloat(centroid[i].data[j].fromLocation.lat);
                var lng1 = parseFloat(centroid[i].data[j].fromLocation.lng);
                var lat2 = parseFloat(centroid[i].data[j].toLocation.lat);
                var lng2 = parseFloat(centroid[i].data[j].toLocation.lng);
                totalDist += _calDirectDist(lat1, lng1, lat2, lng2);
            }
            centroid[i].averageDist = parseFloat((totalDist/centroid[i].num).toFixed(1));
            if (centroid[i].averageDist>max_AvgDist){
              max_AvgDist=centroid[i].averageDist;
            }
        }
        $('#mostDist').trigger('configure', {
//          max: centroid_i.num
          max: max_AvgDist
        });

        if (map.getZoom()>13){
          showMarker();
          hideLabel();
        } else {
          hideMarker();
          showLabel();
        }
    })
    .done(function(){setTimeout(function(){document.getElementById('mapOverlay').className = 'overlay hidden';document.getElementById('tableOverlay').className = 'overlay hidden';},2000);})
    .fail(function(jqXHR, textStatus, errorThrown){setTimeout(function(){document.getElementById('mapOverlay').className = 'overlay hidden';document.getElementById('tableOverlay').className = 'overlay hidden';document.getElementById('mapOverlayF').className = 'overlay';document.getElementById('tableOverlayF').className = 'overlay';},2000);});
}

function _calDirectDist(lat1, lng1, lat2, lng2){
    Number.prototype.toRad = function(){ return this*Math.PI/180;}
    var R = 6371; // km
    var x1 = lat2 - lat1;
    var dLat = x1.toRad();
    var x2 = lng2 - lng1;
    var dLon = x2.toRad();
    var a = Math.sin(dLat/2)* Math.sin(dLat/2) +
            Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var distance = R*c;

    return distance; //km
}
    


function _calcDist(latlng1, latlng2,z,callback) {
//z = 0 when call this function, latlng are new Google.map.latlngs
  var request = {
    origin:latlng1,
    destination:latlng2,
    travelMode: google.maps.TravelMode.DRIVING
  };
  if (z > 4) {
    z = 0;
  }
  directionsService.route(request, function(result, status) {     
  //need to add (var directionsService = new google.maps.DirectionsService();) in the html sc
    var ret;
    if (status == google.maps.DirectionsStatus.OK) {
      ret = result.routes[0].legs[0].distance.value/1000;
      return callback(ret); //in km
    } else if (status == "OVER_QUERY_LIMIT"){
      setTimeout(function(){calcDist(latlng1, latlng2,z+1.5,callback)},z*1000);
    } else {
      return 0;
    }
  });
}

function _calFee(distance){ //in km
  var fee = 22;
  if (distance == 0){
    return 0;
  }
  if (distance <= 2){
    return fee;
  } else {
    distance = distance - 2;
    while (distance > 0){
      distance = distance - 0.2;
      if (fee >= 77.5){
        fee = fee+1;
      } else {
        fee = fee+1.6;
      }
    }
    return Math.round(fee*10)/10;
  }
}

function drawCircle(center,radius,map,clusterColor,strokeColor,strokeWeight){
  return new google.maps.Circle({
    strokeColor: strokeColor,
    strokeOpacity: 0.8,
    strokeWeight: strokeWeight,
    fillColor: clusterColor,
    fillOpacity: 0.4,
    map: map,
    center: center,
    radius: radius
  });
}

function inRange(ranges, value) {
//################################
// Check if an integer with in one of the ranges
//################################
//console.log(ranges,value);
  var dateVal = new Date(value*1000);
  var start = new Date(ranges.start*1000);
  if (dateVal.getDay() == start.getDay() && dateVal.getHours()>=start.getHours() && dateVal.getHours() < parseInt(start.getHours())+3){
    console.log('True');
    return true;
  } else return false;
    /*return ranges.some(function (a) {
        return value >= a[0] && value <= a[1];
    });*/
}

function norm2Unix(normTime) {
  var timeStamp = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + normTime.slice(6,8) + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  var input_date = new Date(timeStamp);
  console.log("input_date = ", input_date);

  var year = normTime.slice(0, 4);
  var month = normTime.slice(4, 6);
  var hour = normTime.slice(8,10);
  
  var date = new Date(year+"-"+ month+ "-" + "01" + " " + hour + ":00:00");
  var days = [];

   while(date.getDay() !==input_date.getDay()){ 
      date.setDate(date.getDate()+1);
   }

   while (date.getMonth()+1 === eval(month)) {
      var temp_date1 = new Date(date);
      var temp_date2 = new Date(date);
      temp_date1.setHours(temp_date1.getHours());
      temp_date2.setHours(temp_date2.getHours()+3);

      temp = [temp_date1.getTime()/1000, temp_date2.getTime()/1000];
      days.push(temp);
      date.setDate(date.getDate() + 7);
   }
   return days;
}

function makeInfoWin(centroid_i){
  var ret = [[0,''],[0,'']];
  jQuery.each(centroid_i.toLocation, function(key, val){
    if (val>ret[0][0]) {
      ret[1][0]=ret[0][0];
      ret[1][1]=ret[0][1];
      ret[0][0] = val;
      ret[0][1] = key;
    } else if (val>ret[1][0] && val<=ret[0][0]){
      ret[1][0] = val;
      ret[1][1] = key;
    }
  });
  return new google.maps.InfoWindow(
    {
      content: "Number of call: " + centroid_i.num + "<br>Sucessful rate: " + (centroid_i.done*100/centroid_i.num).toFixed(1) + "\%" + "<br>Mostly to: " + ret[0][1] + " (" + ret[0][0] + ")"
    }
  );
}

var opened_info = new google.maps.InfoWindow();
function addInfoBox(polygon, center, centroid_i){
  google.maps.event.addListener(polygon, "click", function(){
    opened_info.close();
    
    centroid_i.infowindow.setPosition(center);
    centroid_i.infowindow.open(map);
    opened_info = centroid_i.infowindow;
  });  
}
function addClusterInfo(polygon, centroid_i, maxNumCluster, center){
  google.maps.event.addListener(polygon, "click", function(){
    map.setCenter(center);
    map.setZoom(17);
    if (chosenCluster){
      chosenCluster.setMap(null);
    }
    document.getElementById('clusterOverlayN').className='onerlay hidden';
    document.getElementById('clusterOverlay').className='overlay';
    centroid_i.marker.setMap(map);
    chosenCluster = centroid_i.marker;
    chosenCluster.setOpacity(1);
    if (document.getElementById('ClusterBox').className == 'box box-success collapsed-box'){
      document.getElementById('ClusData_collapse').click();
    }
    var SR = Math.round(centroid_i.done*100/centroid_i.num);
    $('#successRate').val(SR).trigger('change');
    var toDistData = [];
    var ret = [[0,''],[0,'']];
    jQuery.each(centroid_i.toLocation, function(key, val){
      if (val>ret[0][0]) {
        ret[1][0]=ret[0][0];
        ret[1][1]=ret[0][1];
        ret[0][0] = val;
        ret[0][1] = key;
      } else if (val>ret[1][0] && val<=ret[0][0]){
        ret[1][0] = val;
        ret[1][1] = key;
      }
      
      var newData={'label': key,'value': val};
      toDistData.push(newData);
    });
    toDistChart.setData(toDistData);
    // calculate the average distance within cluster
    /*var totalDist = 0;
    for (var i = 0; i< centroid_i.num; i++){
        var lat1 = parseFloat(centroid_i.data[i].fromLocation.lat);
        var lng1 = parseFloat(centroid_i.data[i].fromLocation.lng);
        var lat2 = parseFloat(centroid_i.data[i].toLocation.lat);
        var lng2 = parseFloat(centroid_i.data[i].toLocation.lng);
        totalDist += _calDirectDist(lat1, lng1, lat2, lng2);
    }
    var averageDist = parseFloat((totalDist/centroid_i.num).toFixed(1));
    console.log("averageDisr =", totalDist/centroid_i.num);
    console.log("averageDist = ", averageDist);
    // end calculation
    */

    if (centroid_i.num < maxNumCluster){
      $('#ClusterCall').trigger('configure', {
        max: maxNumCluster
      }); 
    } else {
      $('#ClusterCall').trigger('configure', {
        max: centroid_i.num
      }); 
    }
    $('#ClusterCall').val(centroid_i.num).trigger('change');
//    $('#mostDist').val(ret[0][0]).trigger('change');
    $('#mostDist').val(centroid_i.averageDist).trigger('change');
    document.getElementById('mostDistDesc').innerHTML = 'Average Taxi Ride Distance (km)' ;
    
    setTimeout(function(){document.getElementById('clusterOverlay').className='overlay hidden';},500);
    console.log(chosenCluster);
    fade_chosen_marker();
  });  
}

function fade_chosen_marker(){
  if (markerInterval){
    console.log('clear');
    clearInterval(markerInterval);
  }
  if (markerTimeout){
    clearTimeout(markerTimeout);
  }
  if (chosenCluster!=null){
    if (chosenCluster.opacity>0.2){
      markerTimeout = setTimeout(function(){
          markerInterval = setInterval(function(){
            if (chosenCluster.opacity>0.2){
              chosenCluster.opacity-=0.01
              chosenCluster.setOpacity(Math.max(0, chosenCluster.opacity));
            } else {
              clearInterval(markerInterval);
            }
          },50);
        },1500);
    }
  }
}
function restore_chosen_marker(){
  if (markerInterval){
    clearInterval(markerInterval);
  }
  if (markerTimeout){
    clearTimeout(markerTimeout);
  }
  if (chosenCluster){
    chosenCluster.setOpacity(1);
  }
}
function showMarker(){
  for (var j in markerSet){
    markerSet[j].setMap(map);
  }
}
function showLabel(){
  for (var j in mapLabelSet){
    mapLabelSet[j].setMap(map);
  }
}

function hideMarker(){
  for (var j in markerSet){
    markerSet[j].setMap(null);
  }
}
function hideLabel(){
  for (var j in mapLabelSet){
    mapLabelSet[j].setMap(null);
  }
}
