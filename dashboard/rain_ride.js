function rain_ride(){
  var month = document.getElementById("sel_options").innerHTML.split(" ")[1];
  console.log("month = ", month);
  g = new Dygraph(document.getElementById("rainfall_chart"),
        "../rain_ride/" + month.slice(0, -1) + "-rain_ride.csv",
        {
          title: "Relation between Taxi-ride and Rainfall",
          labels:['Date', '# Ride', 'Rain(mm)'],
          ylabel: 'Number of call',
          y2label: 'Amount of rainfall(mm)',
          fillGraph: "True",
          series : {
            'Rain(mm)':{
              axis: 'y2'
            }
          },
          axes:{
            y2: {
              labelsKMB: true,
              drawGrid: true,
              independentTicks: true,
              gridLinePattern: [2,2]
            }
          }
        }
      );
}