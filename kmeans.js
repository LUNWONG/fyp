function kmeans( arrayToProcess, Clusters )
{

  var Groups = new Array();
  Centroids = new Array();
  var oldCentroids = new Array();
  var changed = 10;  // delta to stop the algo.

  // order the input array
//  arrayToProcess.sort(function(a,b){return a - b})  
  
  // initialise group arrays
  for( initGroups=0; initGroups < Clusters; initGroups++ )
  {
  
    Groups[initGroups] = new Array();

  }  
 //################################################################# 
  // pick initial centroids
  
  initialCentroids=Math.round( arrayToProcess.length/(Clusters+1) );  
  
  for( i=0; i<Clusters; i++ )
  {
  
    Centroids[i]=arrayToProcess[ (initialCentroids*(i+1)) ];
  
  }
  
 //################################################################# 
  do
  {
    for( j=0; j<Clusters; j++ )
	{
	
	  Groups[j] = [];
	
	}
  
// Assign the data point into different cluster group	
	for( i=0; i<arrayToProcess.length; i++ )
	{

	  Distance=-1;
	  oldDistance=-1
	
 	  for( j=0; j<Clusters; j++ )
	  {
	  
//		  distance = Centroids[j] - arrayToProcess[i];
        var x1 = Centroids[j][0];
        var x2 = arrayToProcess[i][0];
        var y1 = Centroids[j][1];
        var y2 = arrayToProcess[i][1];
		
        distance = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
		if ( oldDistance==-1 )
		{
		
		   oldDistance = distance;
		   newGroup = j;
		
		}
		else if ( distance <= oldDistance )
		{
		  
		    newGroup=j;
			oldDistance = distance;
		  
		}
	  
	  }	
	  
	  Groups[newGroup].push( arrayToProcess[i] );	  
	
	}
  
 //################################################################# 
    oldCentroids=Centroids;
  
    for ( j=0; j<Clusters; j++ )
	{
  
      total=0;
	  newCentroid=0;
	  
	  for( i=0; i<Groups[j].length; i++ )
	  {
	  
	    total+=Groups[j][i];
	  
	  } 
	
	  newCentroid=total/Groups[j].length;  
	  
	  Centroids[j]=newCentroid;
	  
	}
  
    for( j=0; j<Clusters; j++ )
	{
	
	  if ( Centroids[j]!=oldCentroids[j] )
	  {
	  
	    changed=Math.sqrt((Centroids[j][0]-oldCentroids[j][0])*(Centroids[j][0] - oldCentroids[j][0]) + (Centroids[j][1] - oldCentroids[j][1])*(Centroids[j][1] - oldCentroids[j][1]));
	  
	  }
	
	}
  
  }
  while( changed>0.0000003 );
  
  return Groups;
  
}
