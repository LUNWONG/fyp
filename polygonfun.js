google.maps.Polygon.prototype.contains = function(point) {
  var j=0;
  var oddNodes = false;
  var x = point.lng();
  var y = point.lat();

  var paths = this.getPath();

  for (var i=0; i < paths.getLength(); i++) {
    j++;
    if (j == paths.getLength()) {j = 0;}
    if (((paths.getAt(i).lat() < y) && (paths.getAt(j).lat() >= y))
    || ((paths.getAt(j).lat() < y) && (paths.getAt(i).lat() >= y))) {
      if ( paths.getAt(i).lng() + (y - paths.getAt(i).lat()) / (paths.getAt(j).lat()-paths.getAt(i).lat()) * (paths.getAt(j).lng() - paths.getAt(i).lng()) < x ) {
        oddNodes = !oddNodes
      }
    }
  }
  return oddNodes;
}

google.maps.Polyline.prototype.contains = google.maps.Polygon.prototype.contains;
var color= ["#ffcc00", "#458B74", "#838B8B", "#0000FF", "#8A2BE2", "#FF0000", "#7FFF00", "#D2681E", "#6495ED",
"#996633", "#000000", "#8470FF", "#FF00FF", "#0000CD", "#3CB371", "#C71585", "#191970", "#8B2500"];

var googleOptions = {
    strokeColor: "#FF0000",
    strokeOpacity: 1,
    strokeWeight: 3,
    fillColor: "#FF0000",
    fillOpacity: 1
};
var districtcenter = {'Central and Western': new google.maps.LatLng(22.279995, 114.142822), 'Eastern': new google.maps.LatLng(22.276532, 114.221233), 'Islands': new google.maps.LatLng(22.261492, 113.952612), 'Kowloon City': new google.maps.LatLng(22.324706, 114.184261), 'Kwai Tsing': new google.maps.LatLng(22.351882, 114.113457), 'Kwun Tong': new google.maps.LatLng(22.316593, 114.231986), 'North': new google.maps.LatLng(22.509961, 114.154696), 'Sai Kung': new google.maps.LatLng(22.351375, 114.235030) , 'Sha Tin': new google.maps.LatLng(22.400086, 114.185831), 'Sham Shui Po': new google.maps.LatLng(22.333633, 114.153574), 'Southern': new google.maps.LatLng(22.234802, 114.206653), 'Tai Po': new google.maps.LatLng(22.446616, 114.153225), 'Tsuen Wan': new google.maps.LatLng(22.385228, 114.077848), 'Tuen Mun': new google.maps.LatLng(22.397678, 113.977914), 'Wan Chai': new google.maps.LatLng(22.269438, 114.178358), 'Wong Tai Sin': new google.maps.LatLng(22.342826, 114.200899), 'Yau Tsim Mong': new google.maps.LatLng(22.314676, 114.168973), 'Yuen Long': new google.maps.LatLng(22.463065, 114.025394)};
//console.log(districtcenter);
var mappoly = [];

function loaddata(){
  districts = {};
  jQuery.getJSON("district_full.json", function(dcdata){
    for (var item=0; item<dcdata.length; item++){
        var dataitem = dcdata[item].GeoJSON;
        dataitem.properties = {"ID": dcdata[item].ename};  //district name

        googleOptions.fillColor = color[item];
        googleOptions.strokeColor = color[item];
        var googleVector = new GeoJSON(dataitem, googleOptions);
        var ename = dcdata[item].ename;
        districts[dcdata[item].ename] = {};
        districts[dcdata[item].ename].polygons = googleVector;
        districts[dcdata[item].ename].ys = 0;
        districts[dcdata[item].ename].no = 0;
        districts[dcdata[item].ename].destys = 0;
        districts[dcdata[item].ename].destno = 0;
        districts[dcdata[item].ename].fromdist = {'Central and Western': 0, 'Eastern': 0, 'Islands': 0, 'Kowloon City': 0, 'Kwai Tsing': 0, 'Kwun Tong': 0, 'North': 0, 'Sai Kung': 0, 'Sha Tin': 0, 'Sham Shui Po': 0, 'Southern': 0, 'Tai Po': 0, 'Tsuen Wan': 0, 'Tuen Mun': 0, 'Wan Chai': 0, 'Wong Tai Sin': 0, 'Yau Tsim Mong': 0, 'Yuen Long': 0};
        districts[dcdata[item].ename].todist = {'Central and Western': 0, 'Eastern': 0, 'Islands': 0, 'Kowloon City': 0, 'Kwai Tsing': 0, 'Kwun Tong': 0, 'North': 0, 'Sai Kung': 0, 'Sha Tin': 0, 'Sham Shui Po': 0, 'Southern': 0, 'Tai Po': 0, 'Tsuen Wan': 0, 'Tuen Mun': 0, 'Wan Chai': 0, 'Wong Tai Sin': 0, 'Yau Tsim Mong': 0, 'Yuen Long': 0};
        districts[dcdata[item].ename].center = districtcenter[dcdata[item].ename];
    }
    //console.log(districts);
    matchdata(districts);
    return districts;
  });
}

function matchdata(districts){
  jQuery.getJSON("district-2015-01-data.json",function(data){
    jQuery.each(data, function(key, val) {
      var coor = new google.maps.LatLng(val.fromLocation.lat,val.fromLocation.lng);
      var destcoor = new google.maps.LatLng(val.toLocation.lat,val.toLocation.lng);
      if (val.fromdistrict != null && val.todistrict != null){
        if (val.status == "Done"){
            districts[val.fromdistrict].ys ++;
            districts[val.todistrict].destys++;
        } else {
            districts[val.fromdistrict].no ++;
            districts[val.todistrict].destno++;
        }
        districts[val.fromdistrict].todist[val.todistrict]++;
        districts[val.todistrict].fromdist[val.fromdistrict]++;
      }
      
    });
    setInfoWin(districts);
    refreshData('Total Data By Source');
    dataMenuSel('Total Data By Source');
  });
}

var colorscale = {'0': '#008080','2': '#66b890','4': '#beecb5','6': '#ffc7c4','8': '#e35875','10': '#8b0000'};
function setInfoWin(districts){
  var counter;       //number of polygon in a district
  for (district in districts){
    var mostFrom = getMost(districts, district, 'fromdist');
    var mostTo = getMost(districts, district, 'todist');
    var center = districts[district].center;    
    districts[district].infowindow = makeInfoWin(district,districts[district].ys,districts[district].no,mostFrom,mostTo);
    if (districts[district].polygons.length>0){
      for (counter = 0; counter<districts[district].polygons.length; counter++){
        addInfoBox(districts[district].polygons[counter],center,districts[district]);
      }
    } else {
      addInfoBox(districts[district].polygons,center,districts[district]);
    }
  }
  

}


var opened_info = new google.maps.InfoWindow();
var chosen_polygons;
function addInfoBox(polygon, center, district){
  google.maps.event.addListener(polygon, "click", function(){
    opened_info.close();
    district.infowindow.setPosition(center);
    district.infowindow.open(map);
    opened_info = district.infowindow;
  });  
}

function showInfoBox(district){
  map.setZoom(13);
  map.setCenter(districts[district].center);
  opened_info.close();
  districts[district].infowindow.setPosition(districts[district].center);
  districts[district].infowindow.open(map);
  opened_info = districts[district].infowindow;
}
function overDistrictMenu(name){
  if (opened_info.name != name){
    map.setZoom(10);
    map.setCenter(hongkong);
  }
  chosen_polygons = [];
  if (districts[name].polygons.length > 0){
    for (var pol = 0; pol < districts[name].polygons.length; pol++){
      districts[name].polygons[pol].setMap(null);
      districts[name].polygons[pol].strokeWeight = 6;
      districts[name].polygons[pol].strokeOpacity = 1;
      districts[name].polygons[pol].fillOpacity = 0.9;
      chosen_polygons.push(districts[name].polygons[pol]);
      districts[name].polygons[pol].setMap(map);
    }
  } else {
    districts[name].polygons.setMap(null);
    districts[name].polygons.strokeWeight = 6;
    districts[name].polygons.fillOpacity = 0.9;
    districts[name].polygons.strokeOpacity = 1;
    chosen_polygons.push(districts[name].polygons);
    districts[name].polygons.setMap(map);
  }
}

function polygonNormal(){
  for (var temp = 0; temp < chosen_polygons.length; temp++){
    chosen_polygons[temp].setMap(null);
    chosen_polygons[temp].strokeWeight = 3;
    chosen_polygons[temp].strokeOpacity = 1;
    chosen_polygons[temp].fillOpacity = 1;
    chosen_polygons[temp].setMap(map);
  }
}


function makeInfoWin(dname, ys, no, mostFrom, mostTo){
  return new google.maps.InfoWindow(
    {
      content: "name: " + dname + "<br>" + "yes: " + districts[district].ys + "<br>" + "no: " + districts[district].no + "<br>Mostly From:<br>1. " + mostFrom[0][1] + "(" + mostFrom[0][0] + ")" + "<br>2. " + mostFrom[1][1] + "(" + mostFrom[1][0] + ")" + "<br>Mostly To:<br>1. " + mostTo[0][1] + "(" + mostTo[0][0] + ")" + "<br>2. " + mostTo[1][1] + "(" + mostTo[1][0] + ")",
      name: dname
    }
  );
}
function propTotal(ys, no, totalys, totalno){
  var prop = (ys+no)/(totalys+totalno)*100;
  var selcolor;
  for (color in colorscale){
    if (prop >= parseFloat(color)){
      selcolor = colorscale[color];
    }
  }
  //console.log(selcolor);
  return selcolor;
}
function getTotalData(districts){
  var totalData = {ys: 0,no: 0};
  jQuery.each(districts, function(totkey,totval){
    totalData.ys += totval.ys;
    totalData.no += totval.no;
  });
  return totalData;
}
function getMost(districts,district,fromto){
  var ret = [[0,''],[0,'']];
  jQuery.each(eval('districts[district].'+ fromto ), function(key, val){
    if (val>ret[0][0]) {
      ret[1][0]=ret[0][0];
      ret[1][1]=ret[0][1];
      ret[0][0] = val;
      ret[0][1] = key;
    } else if (val>ret[1][0] && val<=ret[0][0]){
      ret[1][0] = val;
      ret[1][1] = key;
    }
  });
  return ret;

}
var DataOpt = ['Done Rate','Total Data By Source', 'Total Data By Destination'];
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
function loadDistMenu(){
  document.getElementById("districtsmenu").innerHTML = "";
  document.getElementById("districtsmenu2").innerHTML = "";
  

  var count = 0;
  for (district in districts){
    var color;
    try {
      color = districts[district].polygons[0].strokeColor;
    } catch(err) {
      color = districts[district].polygons.strokeColor;
    }
    var rgbcolor = hexToRgb(color);
    count++;
    console.log(district);
    if(count <=9){
      document.getElementById("districtsmenu").innerHTML += "<p onmouseover=\"overDistrictMenu('" + district+ "')\" onmouseout=\"polygonNormal()\" onClick=\"showInfoBox('" + district + "')\" style=\"text-align: center; height: 8%; color: rgb(" + rgbcolor.r + "," + rgbcolor.g + "," + rgbcolor.b + ");\">" + district + "</p>";
    }else{
      document.getElementById("districtsmenu2").innerHTML += "<p onmouseover=\"overDistrictMenu('" + district+ "')\" onmouseout=\"polygonNormal()\" onClick=\"showInfoBox('" + district + "')\" style=\"text-align: center; height: 8%; color: rgb(" + rgbcolor.r + "," + rgbcolor.g + "," + rgbcolor.b + ");\">" + district + "</p>";

    }

  }
  for (opt in DataOpt){
    document.getElementById("datamenu").innerHTML += "<p id='" + DataOpt[opt] + "' onClick=\"refreshData('" + DataOpt[opt] + "'),dataMenuSel('" + DataOpt[opt] + "')\">" + DataOpt[opt] + "</p>";
  }
}

function dataMenuSel(option){
  console.log(option);
  for (opt in DataOpt){
    if (DataOpt[opt] == option){
      document.getElementById(DataOpt[opt]).style.color = "red";
    } else {
      document.getElementById(DataOpt[opt]).style.color = "black";
    }

  }

}
function propDone(ys,no){
  var selcolor;
  var prop = ys/(ys+no)*100;
  for (color in colorscale){
    if ((100-prop) >= (parseFloat(color)*3.5+5)){
      selcolor = colorscale[color];
    }
  }
  //console.log(selcolor);
  return selcolor;
}
var piedata;
var current_chart;
function refreshData(option){
  map.setZoom(11);
  map.setCenter(hongkong);
  opened_info.close();
  removeMapPoly();
  mappoly = [];
  var totalData = {};
  var fun;
  //console.log(option);
  if (option == 'Done Rate') {
    console.log(option);
    fun = "propDone(district.ys,district.no)";
  } else if (option == 'Total Data By Source'){
    console.log(option);
    totalData = getTotalData(districts);
    fun = "propTotal(district.ys,district.no,totalData.ys,totalData.no)";
    piedata = getTotalPieData(['ys','no']);
    try{
      current_chart.destroy();
    } catch(err) {
      console.log(err);
    }
    current_chart = new Chart(document.getElementById("piechart").getContext("2d")).Pie(piedata);
  } else if (option == 'Total Data By Destination'){
    console.log(option);
    totalData = getTotalData(districts);
    fun = "propTotal(district.destys,district.destno,totalData.ys,totalData.no)";
    piedata = getTotalPieData(['destys','destno']);
    try{
      current_chart.destroy();
    } catch(err) {
      console.log(err);
    }
    current_chart = new Chart(document.getElementById("piechart").getContext("2d")).Pie(piedata);
  }
  for (district in districts){
    fillColor(districts[district], fun, totalData);
  }
}
function removeMapPoly(){
  for (var i = 0; i < mappoly.length; i++){
      mappoly[i].setMap(null);
  }
}
function fillColor(district, fun, totalData){
  var fillColor = eval(fun);
  if (district.polygons.length>0){
      for (counter = 0; counter<district.polygons.length; counter++){
        district.polygons[counter].fillColor = fillColor;
        district.polygons[counter].setMap(map);
        mappoly.push(district.polygons[counter]);
      }
    } else {
      district.polygons.fillColor = fillColor;
      district.polygons.setMap(map);
      mappoly.push(district.polygons);

    }
}

var inView = false;

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}
function getTotalPieData(fields){
  piedata = [];
  var asd = 0;
  for (district in districts){
    var dataitem = {value: 0, color: null};
    for (field in fields){
      dataitem.value += eval('districts[district].'+ fields[field]);
    }
    try {
      dataitem.color = districts[district].polygons[0].strokeColor;
    } catch(err) {
      dataitem.color = districts[district].polygons.strokeColor;
    }
    dataitem.label = district;
    console.log(dataitem.color);
    console.log(asd);
    asd++;
    piedata.push(dataitem);
  }
  return piedata;
}

$(window).scroll(function() {
    //console.log(isScrolledIntoView('#piechart'));
    if (isScrolledIntoView('#piechart')) {
        if (inView) { return; }
        inView = true;
        try{
          current_chart.destroy();
        } catch(err) {
          console.log(err);
        }
        current_chart = new Chart(document.getElementById("piechart").getContext("2d")).Pie(piedata);
    } else {
        if (!inView) { return; }
        inView = false; 
        var piechart =document.getElementById("piechart");
        piechart.getContext('2d').clearRect(0, 0, piechart.width, piechart.height); 
        //console.log('out of view');
    }
});
