import urllib2
import json
import ast
xml = "http://www.hko.gov.hk/cis/dailyExtract/dailyExtract_2015.xml"

def getXml():
  r = urllib2.urlopen(xml)
  result = r.read()
#  j = json.loads(result)
  j = ast.literal_eval(result)
  return j

def writeJson(j):
  f = open('rainfall.json', 'a')
  f.write("[")
  f.close()

  for month in range(len(j["stn"]["data"])):
    data = []
    for day in range(0, len(j["stn"]["data"][month]["dayData"])-2):
      rainfall =  j["stn"]["data"][month]["dayData"][day][8]
      if(rainfall[0] =='T'):
        data.append(rainfall)
      else:
        data.append(str(float(rainfall)))

    months = json.dumps({"month": month+1, "data": data})
    with open("rainfall.json", "a") as output:
      output.write(months)
    if (month <11):
      f = open('rainfall.json', 'a')
      f.write(",")
      f.close()

  f = open('rainfall.json', 'a')
  f.write("]")
  f.close()



if __name__ == "__main__":
   j = getXml()
   writeJson(j)
