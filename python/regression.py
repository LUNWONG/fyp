import json
import numpy as np
from datetime import datetime
from sklearn import linear_model, pipeline, preprocessing

data = []

#############################################################
def read_data():
    global data
    with open("../data/jan.json") as f:
        data = json.load(f)

#############################################################
def get_X_array():
    #######################################
    #     hour: 0-23                      #
    #     minute: 0-59                    #
    #     weekday: 0-6                    #
    #######################################
    global data
    x = []
    for datum in data:
        temp = []
        date = _unixTime2date(int(datum["createdTime"]))
        temp.append(date.hour)
        temp.append(date.minute)
        temp.append(date.date().weekday())
        x.append(temp)
    return x
    

#############################################################
def get_Y_array():
    global data
    y = []
    for x in data:
        temp = []
        try:
            temp.append(float(x["fromLocation"]["lat"]))
        except ValueError,e:
            temp.append(0)
        try:
            temp.append(float(x["fromLocation"]["lng"]))
        except ValueError,e:
            temp.append(0)
        y.append(temp)
    return y

#############################################################
def remove_dummy_data(x, y):
    #######################################
    #     remove the element x, y if its value is 0
    #######################################

    for i, item in enumerate(y):
        if item[0]==0 or item[1]==0:
            del y[i]
            del x[i]
    return x, y

#############################################################
def shuffle_data(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

#############################################################
def _unixTime2date(unix):
    time = datetime.fromtimestamp(unix)
    return time

#############################################################
def linear_regression(X_train, Y_train, X_test, Y_test):
    print "inside regression()"
    regr = linear_model.LinearRegression()
    regr.fit(X_train, Y_train)
    print ("Mean squared error: %.9f" % np.mean((regr.predict(X_test) -Y_test)**2))


#############################################################
def multi_regression(X_train, Y_train, X_test, Y_test):
    linear_regression = linear_model.LinearRegression()
    polynomial_features = preprocessing.PolynomialFeatures(degree=2)
    pipe = pipeline.Pipeline([("polynomial_features", polynomial_features),
                            ("linear_regression", linear_regression)])
    pipe.fit(X_train, Y_train)
    print ("Mean squared error: %.9f" % np.mean((pipe.predict(X_test) -Y_test)**2))

#############################################################
def main():
    read_data()
    x = get_X_array()
    y = get_Y_array()
    x, y = remove_dummy_data(x, y)
    x = np.asarray(x)
    y = np.asarray(y)

    x, y = shuffle_data(x, y)

    X_train = x[:-20]
    X_test = x[-20:]
    Y_train = y[:-20]
    Y_test = y[-20:]

    linear_regression(X_train, Y_train, X_test, Y_test)
    multi_regression(X_train, Y_train, X_test, Y_test)


if __name__ == "__main__":
    main()
