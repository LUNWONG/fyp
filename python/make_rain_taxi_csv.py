import json
from datetime import datetime
from pprint import pprint
daysRide = [0]*31
daysRain = [0]*31

def taxi():
  with open('./2015-data/2015-12-data.json') as f:
    json_ = json.load(f)
  
  for data in json_:
    date = __unixTime2date(data['createdTime'])
    day = date.day
    daysRide[day-1]+=1
  print daysRide

###############################################################

def rainfall():
  with open('./rainfall.json') as f:
    json_ = json.load(f)
  
  for x in range(31):
    rain = json_[11]['data'][x].encode("ascii")
    if (rain == "Trace"):
      daysRain[x] = 0
    else:
      daysRain[x] = rain

###############################################################

def write():
  f=open("rain_ride.csv", "a")
  for x in range(31):
    f.write("2015/12/")
    f.write(str(x+1))
    f.write(",")
    f.write(str(daysRide[x]))
    f.write(",")
    f.write(str(daysRain[x]))
    f.write("\n")
  f.close()

###############################################################

def __unixTime2date(unix):
  time = datetime.fromtimestamp(unix)
  return time.date()
###############################################################
  
if __name__ == "__main__":
  taxi()
  rainfall()
  write()
