import json
import numpy as np
import pydotplus
import cPickle
from datetime import datetime
from sklearn import tree
from IPython.display import Image

data = []

########################################################
def read_data():
    global data
    for i in range(1, 2):
        file_name = "data-"
        file_name += str(i)
        file_name += ".json"
        with open("../data/"+file_name) as f:
            data += json.load(f)

########################################################
def get_X_array():
    global data
    print len(data)
    x = []
    for datum in data:
        temp = []
        date = _uniTime2date(int(datum["createdTime"]))
        temp.append(date.hour)
        temp.append(date.minute)
        temp.append(date.date().weekday())
        x.append(temp)
    return x
########################################################
def get_Y_array():
    global data
    y = []
    for datum in data:
        temp = []
        temp.append(datum["fromCode"])
        y.append(temp)
    return y

########################################################
def shuffle_data(a,b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

########################################################
def _uniTime2date(unix):
    time =datetime.fromtimestamp(unix)
    return time

########################################################
def tree_classifier(X_train, Y_train, X_test, Y_test):
    clf = tree.DecisionTreeClassifier()
    # save the classifier
#    clf = clf.fit(X_train, Y_train)
#    with open("tree_classifier.pkl", "wb") as fid:
#        cPickle.dump(clf, fid)
    with open("tree_classifier.pkl", "rb") as fid:
        clf = cPickle.load(fid)
    pre = clf.predict(X_test)
    correct = 0
    for i in range(20):
        if pre[i][0]==Y_test[i][0][0]:
            correct +=1
        print pre[i] 
        print Y_test[i][0]
        print "--------"
    print correct
#    dot_data = tree.export_graphviz(clf, out_file=None)
#    graph = pydotplus.graph_from_dot_data(dot_data)
#    Image(graph.create_png())
#    graph.write_pdf("taxi_tree.pdf")


def main():
    read_data()
    x = get_X_array()
    y = get_Y_array()
    x = np.asarray(x)
    y = np.asarray(y)

    x, y = shuffle_data(x, y)

    X_train = x[:-20]
    X_test = x[-20:]
    Y_train = y[:-20]
    Y_test = y[-20:]

    tree_classifier(X_train, Y_train, X_test, Y_test)
    

if __name__ =="__main__":
    main()
