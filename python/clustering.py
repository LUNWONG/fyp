import json
import numpy as np
from pprint import pprint
from sklearn.cluster import KMeans

sorted_data = []
############################################################
def find_data(place, startDate, endDate):
     
    #########################################
    #   find out all satisfied data         #
    #   Save in sorted_data                 #
    #########################################
    with open('district-2015-01-data.json') as f:
        data = json.load(f)

    for x in data:
        if "fromdistrict" in x and x["fromdistrict"] ==place and int(x["createdTime"]) >=startDate and int(x["createdTime"]) <=endDate:
            sorted_data.append(x)


################################################################
def get_corr():
    #########################################
    #   Extract the corrdinates data        #
    #   Save in an array                    #
    #########################################
    corr = []
    for x in sorted_data:
        temp = []
        temp.append(str(x["fromLocation"]["lat"]))
        temp.append(str(x["fromLocation"]["lng"]))
        temp = map(float, temp)
        corr.append(temp)

    corr = np.asarray(corr)
    print len(corr)
    return corr
        
################################################################
        
def kmean(corr):
    kmeans = KMeans(n_clusters = 3).fit(corr)
    print kmeans.cluster_centers_

################################################################
def cal_stat():
    exit()


################################################################
def main(place, startDate, endDate):
    find_data(place, startDate, endDate)
    corr = get_corr()
    kmean(corr)
    cal_stat()
   

################################################################
if __name__ == "__main__":
    place = "Sha Tin"
    startDate = "1421424000"
    endDate = "1421510400"
    main(place,startDate, endDate) 
