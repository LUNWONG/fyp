from __future__ import print_function
import json
import numpy as np
from datetime import datetime

# Import MNIST data
#from tensorflow.examples.tutorials.mnist import input_data
#mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import tensorflow as tf

# Parameters
learning_rate = 0.001
training_epochs = 1000
batch_size = 100
display_step = 1

# Network Parameters
n_hidden_1 = 256 # 1st layer number of features
n_hidden_2 = 256 # 2nd layer number of features
n_input = 3 # data input, number of attributes 
n_classes = 430 # fromCode

# tf Graph input
x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_classes])

data = []

# Load data
def load_data():
    global data
    for i in range(1,2):
        print ("i file done\n")
        file_name = "data-"
        file_name += str(i)
        file_name += ".json"
        with open("../data/"+file_name) as f:
            data += json.load(f)

    temp_x = []    
    temp_y = []
    for datum in data:
        temp1 = []
        temp2 = []

        date = _unixTime2date(int(datum["createdTime"]))

        temp1.append(date.hour)
        temp1.append(date.minute)
        temp1.append(date.date().weekday())
        temp_x.append(temp1)
        
        temp2.append(datum["fromCode"])
        temp_y.append(temp2)

    arr_x, arr_y = np.asarray(temp_x), np.asarray(temp_y) #arr_y is (n,1) array which is wrong
    arr_y = _encode_label(arr_y)
    arr_y = _make_label_array(arr_y, len(data))
    arr_x = arr_x.astype("float32")
    arr_y = arr_y.astype("float32")
    arr_x, arr_y = _shuffle_data(arr_x, arr_y)

    train_X = arr_x[:-int(len(data)*0.3)]
    test_X = arr_x[-int(len(data)*0.3):]
    train_y = arr_y[:-int(len(data)*0.3)]
    test_y = arr_y[-int(len(data)*0.3):]

    return train_X, train_y, test_X, test_y

# Covert the unixTime to normal timeStamp
def _unixTime2date(unix):
    time =datetime.fromtimestamp(unix)
    return time

# Shuffle the ordered dataset
def _shuffle_data(a,b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

# Encode the string labels to integer
def _encode_label(y):
    u, indices = np.unique(y, return_index = True)
    return np.searchsorted(u, y)

# Convert the nby1 label array to nby430 array
def _make_label_array(y, no_input):
    label = np.zeros(shape=(no_input, 430))
    for i in range(no_input):
        label[i][y[i]]=1

    return label

# Create batches    
def data_iterator(data, label):
    batch_idx = 0
    while True:
        idxs = np.arange(0, label.shape[0])
        np.random.shuffle(idxs)
        shuf_data = data[idxs]
        shuf_labels = label[idxs]
        batch_size = 50
        for batch_idx in range(0, label.shape[0], batch_size):
            images_batch = shuf_data[batch_idx:batch_idx+batch_size,:]
            labels_batch = shuf_labels[batch_idx:batch_idx+batch_size,:]
            yield images_batch, labels_batch

# Create model
def multilayer_perceptron(x, weights, biases):
    # Hidden layer with RELU activation
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer_1 = tf.nn.relu(layer_1)
    # Hidden layer with RELU activation
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    layer_2 = tf.nn.relu(layer_2)
    # Output layer with linear activation
    out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
    return out_layer

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

#####################################################################

# Load the data
train_X, train_y, test_X, test_y = load_data()
print ("DONE")

# Construct model
pred = multilayer_perceptron(x, weights, biases)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Initializing the variables
#init = tf.global_variables_initializer()
init = tf.initialize_all_variables()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)

    # Training cycle
    for epoch in range(training_epochs):
        avg_cost = 0.
#        total_batch = int(mnist.train.num_examples/batch_size)
        total_batch = 100
        # Loop over all batches
        for i in range(total_batch):
#            batch_x, batch_y = mnist.train.next_batch(batch_size)
            train_x_batch, train_y_batch = data_iterator(train_X, train_y).next()
            # Run optimization op (backprop) and cost op (to get loss value)
#            _, c = sess.run([optimizer, cost], feed_dict={x: batch_x,
            _, c = sess.run([optimizer, cost], feed_dict={x: train_x_batch, y: train_y_batch})
            # Compute average loss
            avg_cost += c / total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", \
                "{:.9f}".format(avg_cost))
    print("Optimization Finished!")

    # Test model
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print("Accuracy:", accuracy.eval(session=sess, feed_dict = {x: test_X, y: test_y}))
