var centroid = {};
var markerSet = [];
var circleSet = [];
function clustering(sel_Time,sel_Dist){
    for (var i in markerSet){
      markerSet[i].setMap(null);
    }
    markerSet = [];
    for (var i in circleSet){
      circleSet[i].setMap(null);
    }
    circleSet = [];
    centroid = {};
    var selected_district = sel_Dist;
    console.log(sel_Dist);
//    var selected_time = norm2Unix("201502032100");
    var time_ranges= sel_Time;
    console.log(time_ranges);
    var startT = new Date(sel_Time.start*1000);
    jQuery.getJSON("../data/district-2015-" + (startT.getMonth()+1) + "-data.json", function(data){
        var dbscan_input = [];
        var filtered_data = [];
        jQuery.each(data, function(key, val){
            if(val.fromdistrict === selected_district && inRange(time_ranges, parseInt(val.createdTime))){
                corrdinates={};
                corrdinates.location = {};
                corrdinates.location.id = val.orderCreatorID;
                corrdinates.location.accuracy = 1;
                corrdinates.location.latitude = val.fromLocation.lat;
                corrdinates.location.longitude = val.fromLocation.lng;
                dbscan_input.push(corrdinates);
                filtered_data.push(val);

                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(val.fromLocation.lat), lng: parseFloat(val.fromLocation.lng)},
                    map: map
                });
                markerSet.push(marker);
            }
        });

        //######################################################
        //
        // Running DBSCAN algorithm
        var dbscanner = jDBSCAN().eps(0.2).minPts(5).distance('HAVERSINE').data(dbscan_input);
        var point_assg_result = dbscanner();
        console.log(point_assg_result);
        console.log("total number of data: ", filtered_data.length);


        //#####################################################
        //
        // Finding the centroid of clusters
        // Pushing the data into the corresponding cluster
        for (var i=0; i<point_assg_result.length; i++){
            if ([point_assg_result[i]] != 0 ){ // 0 means the data is not assigned to any cluster
                if (centroid[point_assg_result[i]]){
                    centroid[point_assg_result[i]].num++;
                    if (filtered_data[i].status == "Done"){
                        centroid[point_assg_result[i]].done ++;
                    }
                    centroid[point_assg_result[i]].cen_lat += parseFloat(dbscan_input[i].location.latitude);
                    centroid[point_assg_result[i]].cen_lng += parseFloat(dbscan_input[i].location.longitude);
                    centroid[point_assg_result[i]].data.push(filtered_data[i]);
                }else{
                    centroid[point_assg_result[i]] = {};
                    centroid[point_assg_result[i]].num = 1;
                    if (filtered_data[i].status == "Done"){
                        centroid[point_assg_result[i]].done = 1;
                    }
                    else {
                        centroid[point_assg_result[i]].done = 0;
                    }
                    centroid[point_assg_result[i]].cen_lat = parseFloat(dbscan_input[i].location.latitude);
                    centroid[point_assg_result[i]].cen_lng = parseFloat(dbscan_input[i].location.longitude);
                    centroid[point_assg_result[i]].data = [];
                    centroid[point_assg_result[i]].data.push(filtered_data[i]);
                }
            }
        }


        //######################################################
        //
        // Calculate the real centroid position
        for (var j in centroid){
            centroid[j].cen_lat = centroid[j].cen_lat/centroid[j].num;
            centroid[j].cen_lng = centroid[j].cen_lng/centroid[j].num;
        }


        //######################################################
        //
        // Plot the clusters circle and Find the distance of each order
        var info =[];
        for (var i in centroid){
            circle = drawCircle({lat: centroid[i].cen_lat, lng: centroid[i].cen_lng}, 300, map);
            circleSet.push(circle);
            centroid[i].toLocation = {};
            for (var j = 0; j<centroid[i].data.length; j++){
                if(centroid[i].toLocation[centroid[i].data[j].todistrict]){
                    centroid[i].toLocation[centroid[i].data[j].todistrict]++;
                }
                else{
                    centroid[i].toLocation[centroid[i].data[j].todistrict] = 1;
                }
            }
            centroid[i].infowindow = makeInfoWin(centroid[i]);
            addInfoBox(circle, {lat: centroid[i].cen_lat, lng: centroid[i].cen_lng}, centroid[i]);
                
            /*for (var j = 0; j<centroid[i].data.length; j++){
                //console.log(centroid[i].data[j].fromLocation);
                var start = new google.maps.LatLng(centroid[i].data[j].fromLocation.lat,centroid[i].data[j].fromLocation.lng);
                var end = new google.maps.LatLng(centroid[i].data[j].toLocation.lat,centroid[i].data[j].toLocation.lng);
                var distant;
                calcDist(start, end, 1, function(ret){
                    console.log(distant);
                    distant = ret;
                });
                //console.log("distant: ", distant);
            }*/
        }
        console.log(centroid);
    });
}


function _calcDist(latlng1, latlng2,z,callback) {
//z = 0 when call this function, latlng are new Google.map.latlngs
  var request = {
    origin:latlng1,
    destination:latlng2,
    travelMode: google.maps.TravelMode.DRIVING
  };
  if (z > 4) {
    z = 0;
  }
  directionsService.route(request, function(result, status) {     
  //need to add (var directionsService = new google.maps.DirectionsService();) in the html sc
    var ret;
    if (status == google.maps.DirectionsStatus.OK) {
      ret = result.routes[0].legs[0].distance.value/1000;
      return callback(ret); //in km
    } else if (status == "OVER_QUERY_LIMIT"){
      setTimeout(function(){calcDist(latlng1, latlng2,z+1.5,callback)},z*1000);
    } else {
      return 0;
    }
  });
}

function _calFee(distance){ //in km
  var fee = 22;
  if (distance == 0){
    return 0;
  }
  if (distance <= 2){
    return fee;
  } else {
    distance = distance - 2;
    while (distance > 0){
      distance = distance - 0.2;
      if (fee >= 77.5){
        fee = fee+1;
      } else {
        fee = fee+1.6;
      }
    }
    return Math.Round(fee*10)/10;
  }
}

function drawCircle(center,radius,map){
  return new google.maps.Circle({
    strokeColor: '#000000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#000000',
    fillOpacity: 0.4,
    map: map,
    center: center,
    radius: radius
  });
}

function inRange(ranges, value) {
//################################
// Check if an integer with in one of the ranges
//################################
//console.log(ranges,value);
  var dateVal = new Date(value*1000);
  var start = new Date(ranges.start*1000);
  if (dateVal.getDay() == start.getDay() && dateVal.getHours()>=start.getHours() && dateVal.getHours() < parseInt(start.getHours())+3){
    console.log('True');
    return true;
  } else return false;
    /*return ranges.some(function (a) {
        return value >= a[0] && value <= a[1];
    });*/
}

function norm2Unix(normTime) {
  var timeStamp = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + normTime.slice(6,8) + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  var input_date = new Date(timeStamp);
  console.log("input_date = ", input_date);

  var year = normTime.slice(0, 4);
  var month = normTime.slice(4, 6);
  var hour = normTime.slice(8,10);
  
  var date = new Date(year+"-"+ month+ "-" + "01" + " " + hour + ":00:00");
  var days = [];

   while(date.getDay() !==input_date.getDay()){ 
      date.setDate(date.getDate()+1);
   }

   while (date.getMonth()+1 === eval(month)) {
      var temp_date1 = new Date(date);
      var temp_date2 = new Date(date);
      temp_date1.setHours(temp_date1.getHours());
      temp_date2.setHours(temp_date2.getHours()+3);

      temp = [temp_date1.getTime()/1000, temp_date2.getTime()/1000];
      days.push(temp);
      date.setDate(date.getDate() + 7);
   }
   return days;
}

function makeInfoWin(centroid_i){
  var ret = [[0,''],[0,'']];
  jQuery.each(centroid_i.toLocation, function(key, val){
    if (val>ret[0][0]) {
      ret[1][0]=ret[0][0];
      ret[1][1]=ret[0][1];
      ret[0][0] = val;
      ret[0][1] = key;
    } else if (val>ret[1][0] && val<=ret[0][0]){
      ret[1][0] = val;
      ret[1][1] = key;
    }
  });
  return new google.maps.InfoWindow(
    {
      content: "Number of call: " + centroid_i.num + "<br>Sucessful rate: " + (centroid_i.done*100/centroid_i.num).toFixed(1) + "\%" + "<br>Mostly to: " + ret[0][1] + " (" + ret[0][0] + ")"
    }
  );
}

var opened_info = new google.maps.InfoWindow();
function addInfoBox(polygon, center, centroid_i){
  google.maps.event.addListener(polygon, "click", function(){
    opened_info.close();
    
    centroid_i.infowindow.setPosition(center);
    centroid_i.infowindow.open(map);
    opened_info = centroid_i.infowindow;
  });  
}
