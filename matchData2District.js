google.maps.Polygon.prototype.contains = function(point) {
  var j=0;
  var oddNodes = false;
  var x = point.lng;
  var y = point.lat;

  var paths = this.getPath();

  for (var i=0; i < paths.getLength(); i++) {
    j++;
    if (j == paths.getLength()) {j = 0;}
    if (((paths.getAt(i).lat() < y) && (paths.getAt(j).lat() >= y))
    || ((paths.getAt(j).lat() < y) && (paths.getAt(i).lat() >= y))) {
      if ( paths.getAt(i).lng() + (y - paths.getAt(i).lat()) / (paths.getAt(j).lat()-paths.getAt(i).lat()) * (paths.getAt(j).lng() - paths.getAt(i).lng()) < x ) {
        oddNodes = !oddNodes
      }
    }
  }
  return oddNodes;
}

google.maps.Polyline.prototype.contains = google.maps.Polygon.prototype.contains;
var color= ["#8B8378", "#458B74", "#838B8B", "#0000FF", "#8A2BE2", "#FF0000", "#7FFF00", "#D2681E", "#6495ED",
"#FFF8DC", "#000000", "#8470FF", "#FF00FF", "#0000CD", "#3CB371", "#C71585", "#191970", "#8B2500"]

var googleOptions = {
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35
};
function loaddata(){
  districts = {};
  jQuery.getJSON("district_full.json", function(dcdata){
    for (var item=0; item<dcdata.length; item++){
        var dataitem = dcdata[item].GeoJSON;
        dataitem.properties = {"ID": dcdata[item].ename};  //district name

        googleOptions.fillColor = color[item];
        googleOptions.strokeColor = color[item];
        var googleVector = new GeoJSON(dataitem, googleOptions);
        var ename = dcdata[item].ename;
        districts[dcdata[item].ename] = {};
        districts[dcdata[item].ename].polygons = googleVector;
        districts[dcdata[item].ename].ys = 0;
        districts[dcdata[item].ename].no = 0;
    }
    matchdata(districts);
    return districts;
  });
}
var json = "";
function matchdata(districts){
  jQuery.getJSON("data.json",function(data){
    jQuery.each(data, function(key, val) {
      var jump = 0;
      var coor_to = {lat: val.toLocation.lat, lng: val.toLocation.lng};
      var coor_from = {lat: val.fromLocation.lat, lng: val.fromLocation.lng};
      districts:
      for (district in districts){
        var counter; //number of polygon within a district
        for (counter = 0; counter<districts[district].polygons.length;counter++){
            if(districts[district].polygons[counter].contains(coor_to)){
                val.todistrict = district;
                jump = jump + 1;
            }
            if(districts[district].polygons[counter].contains(coor_from)){
                val.fromdistrict = district;
                jump = jump + 1;
            }
            if (jump ==2){
                break districts;
            }
        }
        if (counter == 0){      // There is only only polygon in a district
            if(districts[district].polygons.contains(coor_to)){
                val.todistrict = district;
                jump = jump + 1;
            }
            if(districts[district].polygons.contains(coor_from)){
                val.fromdistrict = district;
                jump = jump + 1;
            }
            if (jump ==2){
                break districts;
            }
        }
      }
      json+=JSON.stringify(val)+",";
    });
    
      writefile("district-2015-02.json", json);
  });
}
//################################################################
var opened_info = new google.maps.InfoWindow();
function showInfo(event){
  opened_info.close();
  this.infowindow.setPosition(event.latLng);
  this.infowindow.open(map);
  opened_info = this.infowindow;
}

  var writefile = (function(filename, data) {
    var blob = new Blob([data], {type: 'text/csv'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
});  
