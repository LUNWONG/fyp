function calcDist(latlng1, latlng2,z,callback) {
//z = 0 when call this function, latlng are new Google.map.latlngs
  var request = {
    origin:latlng1,
    destination:latlng2,
    travelMode: google.maps.TravelMode.DRIVING
  };
  if (z > 4) {
    z = 0;
  }
  directionsService.route(request, function(result, status) {     
  //need to add (var directionsService = new google.maps.DirectionsService();) in the html sc
    var ret;
    if (status == google.maps.DirectionsStatus.OK) {
      ret = result.routes[0].legs[0].distance.value/1000;
      return callback(ret); //in km
    } else if (status == "OVER_QUERY_LIMIT"){
      setTimeout(function(){calcDist(latlng1, latlng2,z+1.5,callback)},z*1000);
    } else {
      return 0;
    }
  });
}

function calFee(distance){ //in km
  var fee = 22;
  if (distance == 0){
    return 0;
  }
  if (distance <= 2){
    return fee;
  } else {
    distance = distance - 2;
    while (distance > 0){
      distance = distance - 0.2;
      if (fee >= 77.5){
        fee = fee+1;
      } else {
        fee = fee+1.6;
      }
    }
    return Math.Round(fee*10)/10;
  }
}

function drawCircle(center,radius,map){
  var Circle = new google.maps.Circle({
    strokeColor: '#000000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#000000',
    fillOpacity: 0.4,
    map: map,
    center: center,
    radius: radius
  });
}

function norm2Unix(normTime){ 
//in String format as ('yyyymmddhhmm')
  var timeStamp_start = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + normTime.slice(6,8) + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  var timeStamp_end = normTime.slice(0,4) + "/" + normTime.slice(4,6) + "/" + (parseInt(normTime.slice(6,8))+1).toString() + " " + normTime.slice(8,10) + ":" + normTime.slice(10,12);
  console.log(normTime,Date.parse(timeStamp_start+" +0800")/1000);
  return {start: Date.parse(timeStamp_start+" +0800")/1000,
        end: Date.parse(timeStamp_end+" +0800")/1000
        };
}
